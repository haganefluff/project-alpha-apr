## Feature 1
* [ ] < - For fast creation keep blank
* [x] Fork and clone the starter project from Project Alpha 
* [x] Create a new virtual environment in the repository directory for the project - python -m venv .venv
* [x] Activate the virtual environment - ./.venv/Scripts/Activate.ps1
* [x] Upgrade pip - python -m pip install --upgrade pip
* [x] Install django - pip install django
* [x] Install black - pip install black
* [x] Install flake8 - pip install flake8
* [x] Install djhtml - pip install djhtml
* [x] Deactivate your virtual environment - deactivate
* [x] Activate your virtual environment (As on line 4)
* [x] Use pip freeze to generate a requirements.txt file - pip freeze > requirements.txt

* [x] Test Feature, Success
* [x] Commit / Push

## Feature 2
* [x] Create a Django project named tracker so that the manage.py file is in the top directory - django-admin startproject «name» .
* [x] Create a Django app named accounts and install it in the tracker Django project in the INSTALLED_APPS list
* [x] Create a Django app named projects and install it in the tracker Django project in the INSTALLED_APPS list
* [x] Create a Django app named tasks and install it in the tracker Django project in the INSTALLED_APPS list - python manage.py startapp «name»
  
* [x] Run the migrations - makemigrations, migrate
* [x] Create superuser - python manage.py createsuperuser
- User: admin | Password: admin
* [x] Test Feature, Sucess
* [x] Commit / Push

## Feature 3
* [x] Create Project model in projects app
* [x] Needs following attributes
* [x] - name / string / 200 char
* [x] - description / str / no max length
* [x] - members / many-to-many / Refers to the auth.User model, related name "projects"
* [x] String conversion method __str__ value name
* [x] Make Migrations

* [x] Test Feature, Sucess
* [x] Commit / Push

## Feature 4
* [x] Register Project model with admin.
* [x] Test Feature, Sucess
* [x] Commit / Push

## Feature 5
* [x] Create list view for Project Model
* [x] Register view path
* [x] register project paths with tracker project
* [x] create base.html for inheirtance
* [x] create HTML template (template folder, projects folder)
* [x] Test Feature, Sucess
* [x] Commit / Push
* I will be create the HTML so that it can be placed in the View (leaving blank) then creating the View so that it calls for the HTML and Model required. I will then finish the task by registering the view with urlpatterns in urls.py
  
  ## Feature 6
  * [x] Use RedirectView so that /projects is redirected to from "home"
  * [x] Set / for /projects as a root directory
  * [x] Test Feature, Sucess
  * [x] Commit / Push

  
## Feature 7
* [x] Register LoginView in accounts.urls with path "login/" name = login
* [x] Include url patterns from accounts app in the tracker project with the prefix "accounts/"
* [x] Create templates directory under accounts
* [x] Create registration directory under templates
* [x] Create HTML template login.html under registration
* [x] Create post form in login.html / HTML inheirtance and follow template specifications
* [x] In tracker settings.py create and set variable LOGIN_REDIRECT_URL to "home"
* [x] Test Features, Sucess
* [x] Commit / Push

## Feature 8
* [x] protect list view for project model where members = logged in user
* [x] change queryset of view to filter Project objects where members = logged in user
* [x] Test Features, Sucess
* [x] Commit / Push

## Feature 9
* [x] As Feature 7, but import Logout instead, will not require HTML
* [x] Test Features, Sucess
* [x] Commit / Push

## Feature 10
* [x] Create view function inside views.py in the account directory
* [x] Must import UserCreationForm from built-in auth forms
* [x] Need to use create_user method to create a new user account from user/password
* [x] will need login function that logs an account in
* [x] after user is created redirect to "home"
* [x] create signup.html in registration directory
* [x] Post form for signup.html, template inheirtance
* [x] Test Features, Sucess
* [x] Commit / Push

## Feature 11
* [x] Task model in tasks directory.
* [x] will require
* [x] name / string / max length 200 characters
* [x] start_date / date-time
* [x] due_date / date-time
* [x] is_completed / boolean, defaults to False
* [x] project / foreign key / refers to projects.Project model, related name "tasks", on_delete.CASCADE
* [x] assignee / foreign key / Refers to auth.User model, null is True, related name "tasks" / on_delete set to null
* [x] python manage.py makemigrations
* [x] python manage.py migrate
* [x] Test Features, Sucess
* [x] Commit / Push

## Feature 12
* [x] import and register the Task model in the task admin.py (admin.site.register)
* [x] Test Features, Sucess
* [x] Commit / Push

## Feature 13
* [ ] Create a view in projects that shows details of a project
* [ ] User must be logged in, will require decorator import
* [ ] In projects.urls, register the view with "<int:pk>/" and name "show_project"
* [ ] Create template to show details of the project and a table of its tasks, use template inheirtance
* [ ] Update list template to show number of tasks for a project
* [ ] Update list template to have a link from the project name to the detail view for that project <a href="{}">