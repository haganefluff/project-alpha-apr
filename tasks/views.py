from django.shortcuts import get_object_or_404, render, redirect
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm, CompleteForm

# Create your views here.


@login_required
def create_task(request):
    form = TaskForm()
    context = {"form": form}
    if request.method == "POST":
        form = TaskForm(request.POST)
    if form.is_valid():
        task = form.save(commit=False)
        task.assignee = request.user
        task.save()
        return redirect("show_project", task.project.id)
    else:
        form = TaskForm()
        context = {"form": form}
    return render(request, "tasks/create.html", context)


@login_required
def show_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks}
    print(tasks)
    return render(request, "tasks/show_task_list.html", context)


@login_required
def complete_task(request, pk):
    complete_item = get_object_or_404(Task, pk=pk)
    if request.method == "POST":
        form = CompleteForm(request.POST, instance=complete_item)
    if form.is_valid():
        task = form.save(commit=False)
        task.assignee = request.user
        task.save()
        return redirect("show_my_tasks")
    else:
        form = CompleteForm()
    return render(request, "tasks/show_task_list.html", {"form": form})
