from django.urls import path
from projects.views import create_project, show_projects, show_project_details

urlpatterns = [
    path("", show_projects, name="list_projects"),
    path("<int:pk>/", show_project_details, name="show_project"),
    path("create/", create_project, name="create_project"),
]
