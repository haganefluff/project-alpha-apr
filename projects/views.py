from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm

# Create your views here.
@login_required
def show_projects(request):
    list_projects = Project.objects.filter(members=request.user)
    context = {"list_projects": list_projects}
    print(list_projects)
    return render(request, "projects/show_projects_list.html", context)


@login_required
def show_project_details(request, pk):
    project = Project.objects.get(members=request.user, pk=pk)
    context = {"project": project}
    return render(request, "projects/project_details.html", context)


@login_required
def create_project(request):
    form = ProjectForm()
    context = {"form": form}
    if request.method == "POST":
        form = ProjectForm(request.POST)
    if form.is_valid():
        project = form.save(commit=False)
        project.user = request.user
        project.save()
        return redirect("show_project", project.id)
    else:
        form = ProjectForm()
        context = {"form": form}
    return render(request, "projects/create.html", context)
